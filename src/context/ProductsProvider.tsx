import { ReactElement, createContext, useState } from "react";
import { useEffect } from "react";
import { BASE_URL } from "../config";
import Cookies from "js-cookie";
export type ProductType = {
  id: number;
  description: string;
  name: string;
  price: number;
  img: string;
};

export type OrderDto = {
  id: number;
  isFinised: Boolean;
  orderProducts: OrderProductDto[];
  time: number;
  totalItems: number;
  totalPrice: number;
};

export type OrderProductDto = {
  id: number;
  product: ProductType;
  quantity: number;
};

export type OrderPreview = {
  id: number;
  totalSum: number;
};

export type UseProductsContextType = { products: ProductType[] };
const initContextState: UseProductsContextType = { products: [] };
const ProductsContext = createContext<UseProductsContextType>(initContextState);
type ChildrenType = { children?: ReactElement | ReactElement[] };

export const ProductsProvider = ({ children }: ChildrenType): ReactElement => {
  const [products, setProducts] = useState<ProductType[]>([]);

  useEffect(() => {
    const fetchProducts = async (): Promise<ProductType[]> => {
      const data = await fetch(`${BASE_URL}/products`, {
        headers: { Authorization: `Bearer ${Cookies.get("token")}` },
      })
        .then((res) => {
          return res.json();
        })
        .catch((err) => {
          if (err instanceof Error) console.log(err.message);
        });
      return data;
    };

    fetchProducts().then((products) => setProducts(products));
  }, []);

  return (
    <ProductsContext.Provider value={{ products }}>
      {children}
    </ProductsContext.Provider>
  );
};

export default ProductsContext;
