type PropsType = {
    totalItems: number;
    totalPrice: number;
  };

const Footer = ({totalItems,totalPrice  }:PropsType) => {

    const year: number = new Date().getFullYear()
    const content = (
        <footer className="footer">
            <p>Total Items: {totalItems}</p>
                <p>Total Price:$ {totalPrice}</p>
                <p>Shopping Cart &copy; {year}</p>
        </footer>
    )

    return content
}
export default Footer