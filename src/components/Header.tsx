import Nav from "./Nav";

type PropsType = {
  totalItems: number;
  totalPrice: number;
};

const Header = ({ totalItems, totalPrice }: PropsType) => {
  const content = (
    <header className="header">
      <div className="header__title-bar">
        <h1>My eshop</h1>
        <div className="header__price-box">
          <p>Total Items: {totalItems}</p>
          <p>Total Price: ${totalPrice} </p>
        </div>
      </div>
      <Nav />
    </header>
  );

  return content;
};
export default Header;
