import React, { ChangeEvent, ReactElement, memo, useState } from "react";
import { CartItemType } from "../context/CartProvider";
import { ReducerAction } from "../context/CartProvider";
import { OrderProductDto } from "../context/ProductsProvider";
import { BASE_URL } from "../config";
import { ToastContainer, toast } from "react-toastify";
import Cookies from "js-cookie";

type PropsType = {
  item: OrderProductDto;
  onQuantityChanged: () => void;
};

type ProductQuantity = {
  quantity: number;
};

const CartLineItem = ({ item, onQuantityChanged }: PropsType) => {
  const [quantity, setQuantity] = useState(item.quantity);

  const img: string = new URL(`../images/${item.product.img}`, import.meta.url)
    .href;

  const highestQty: number = 20 > item.quantity ? 20 : item.quantity;

  const optionValues: number[] = [...Array(highestQty).keys()].map(
    (i) => i + 1
  );
  const options: ReactElement[] = optionValues.map((val) => {
    return (
      <option value={val} key={`opt${val}`}>
        {val}
      </option>
    );
  });

  const onChangeQty = (e: ChangeEvent<HTMLSelectElement>) => {
    const newQuantity = Number(e.target.value);

    if (item.quantity != newQuantity) {
      const requestData: ProductQuantity = {
        quantity: newQuantity,
      };

      const changeQuantity = async (): Promise<ProductQuantity> => {
        const data = await fetch(
          `${BASE_URL}/orders/current/products/${item.product.id}/quantity`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${Cookies.get("token")}`,
            },
            body: JSON.stringify(requestData),
          }
        )
          .then((res) => {
            console.log("kvantita zmenena");
            return res.json();
          })
          .catch((err) => {
            if (err instanceof Error) console.log(err.message);
          });
        return data;
      };

      changeQuantity().then(() => {
        setQuantity(newQuantity);
        onQuantityChanged();
      });
    }
  };

  const onRemoveFromCart = () => {
    const requestData: ProductQuantity = {
      quantity: 0,
    };
    const removeFromOrder = async (): Promise<ProductQuantity> => {
      const data = await fetch(
        `${BASE_URL}/orders/current/products/${item.product.id}/quantity`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${Cookies.get("token")}`,
          },
          body: JSON.stringify(requestData),
        }
      )
        .then((res) => {
          console.log("kvantita zmenena");
          return res.json();
        })
        .catch((err) => {
          if (err instanceof Error) console.log(err.message);
        });
      return data;
    };
    removeFromOrder().then(() => {
      onQuantityChanged();
    });
  };

  const content = (
    <li className="cart__item">
      <img src={img} className="cart__img" />
      <div aria-label="Item Name">{item.product.name}</div>
      <div aria-label="Price per item">
        {new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: "USD",
        }).format(item.product.price)}
      </div>
      <label htmlFor="itemQty" className="offscreen">
        item quantity{" "}
      </label>
      <select
        name="itemQty"
        id="itemQty"
        className="cart__select"
        value={quantity}
        aria-label="Item Quantity"
        onChange={onChangeQty}
      >
        {options}
      </select>
      <div className="cart__item-subtotal" aria-label="Line Item Subtotal">
        {new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: "USD",
        }).format(quantity * item.product.price)}
      </div>
      <button
        className="cart__button"
        onClick={onRemoveFromCart}
        aria-label="Remove Item From Cart"
        title="Remove Item From Cart"
      >
        X
      </button>
      <ToastContainer />
    </li>
  );

  return content;
};

function areItemsEqual(
  { item: prevItem }: PropsType,
  { item: nextItem }: PropsType
) {
  return prevItem.id == nextItem.id;
}
const MemoizedCartLineItem = memo<typeof CartLineItem>(
  CartLineItem,
  areItemsEqual
);
export default CartLineItem;
