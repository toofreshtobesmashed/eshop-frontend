import { useNavigate } from "react-router-dom";

const Nav = () => {
  const navigate = useNavigate();

  const navigateToCart = () => {
    navigate("/cart");
  };
  
  const content = (
    <nav className="nav">
      <button onClick={() => navigateToCart()}>View Cart</button>
    </nav>
  );

  return content;
};
export default Nav;
