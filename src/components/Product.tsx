import { ProductType } from "../context/ProductsProvider";
import { ReactElement, memo } from "react";
import { BASE_URL } from "../config";
import "./Product.css";
import Cookies from "js-cookie";

type PropsType = {
  onAdd: () => void;
  product: ProductType;
  quantity: number|undefined ;
};

const Product = ({ product, quantity, onAdd }: PropsType): ReactElement => {
  const img: string = new URL(`../images/${product.img}`, import.meta.url).href;
  console.log(img);
  console.log(product.img);

  async function onAddToCart() {
    const request = {
      productId: product.id,
      quantity: 1,
    };
    const res = await fetch(`${BASE_URL}/orders/current/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${Cookies.get("token")}`,
      },
      body: JSON.stringify(request),
    });

    if (res.status == 200) {
      console.log("produkt bol uspesne pridany");
      onAdd();
    } else {
      alert(`chyba ${res.status}`);
    }
  }

  const itemInCart = quantity ? ` → Item in Cart: ✔️ ${quantity}x` : null;


  const content = (
    <article className="product">
      <h3>{product.name}</h3>
      <img src={img} alt={product.name} className="product__img" />
      <p>
        {new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: "USD",
        }).format(product.price)}
        {itemInCart}
      </p>
      <button onClick={onAddToCart} className="product_btn">
        Add to Cart
      </button>
    </article>
  );

  return content;
};
 
export default Product ;
