import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import CartLineItem from "./CartLineItem";
import { BASE_URL } from "../config";
import { OrderDto, OrderPreview } from "../context/ProductsProvider";
import "./Cart.css";
import Cookies from "js-cookie";

const Cart = () => {
  const [order, setOrder] = useState<OrderDto>({
    id: 0,
    isFinised: false,
    orderProducts: [],
    time: 0,
    totalItems: 0,
    totalPrice: 0,
  });
  const navigate = useNavigate();

  useEffect(() => {
    loadOrder();
  }, []);

  const navigateToOrder = () => {
    payForOrder();
  };

  const backToProducts = () => {
    navigate("/productList");
  };

  async function loadOrder() {
    const load = async (): Promise<OrderDto> => {
      const data = await fetch(`${BASE_URL}/orders/current`, {
        method: "GET",
        headers: { Authorization: `Bearer ${Cookies.get("token")}` },
      })
        .then((res) => {
          console.log("objednavka bola zobrazena");
          return res.json();
        })
        .catch((err) => {
          if (err instanceof Error) console.log(err.message);
        });
      return data;
    };

    load().then((loadedOrder) => setOrder(loadedOrder));
  }

  async function payForOrder() {
    const getPreview = async (): Promise<OrderPreview> => {
      const data = await fetch(`${BASE_URL}/orders/current/pay`, {
        method: "POST",
        headers: { Authorization: `Bearer ${Cookies.get("token")}` },
      })
        .then((res) => {
          console.log("objednavka bola zaplatena");
          return res.json();
        })
        .catch((err) => {
          if (err instanceof Error) console.log(err.message);
        });
      return data;
    };

    getPreview().then((preview) => {
      navigate("/order", { state: { preview: preview } });
    });
  }

  const pageContent = (
    <>
      <h1 className="cart_title">Cart</h1>
      <ul className="cart">
        {order.orderProducts.map((item) => {
          return (
            <CartLineItem key={item.id} item={item} onQuantityChanged={loadOrder} />
          );
        })}
      </ul>
      <div className="cart__totals">
        <p>total items: {order.totalItems}</p>
        <p>total price:$ {order.totalPrice}</p>
        {order.totalItems > 0 ? (
          <button className="cart__submit" onClick={navigateToOrder}>
            Place Order
          </button>
        ) : (
          <button className="cart__submit" onClick={backToProducts}>
            Back to products
          </button>
        )}
      </div>
    </>
  );
  const content = <main className="main main--cart">{pageContent}</main>;
  return content;
};

export default Cart;
