import React from "react";
import "./Order.css";
import { useLocation, useNavigate } from "react-router-dom";

const Order: React.FC = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const preview = location.state?.preview;

  const backToProducts = () => {
    navigate("/productlist");
  };

  if (!preview) {
    return <div>No order details found</div>;
  }

  return (
    <div className="final_order_container">
      <div className="final_order">
        <p>Thank you for your order</p>
        <span>Your order has an id : {preview.id} </span>
        <br />
        <span>Total sum you paid : {preview.totalSum} €</span>
        <br />
        <button onClick={backToProducts} className="final_order_btn">Return to main page</button>
      </div>
    </div>
  );
};

export default Order;
