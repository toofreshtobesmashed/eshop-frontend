import { useState } from "react";
import "./Login.css";
import { useNavigate } from "react-router-dom";
import { BASE_URL    } from "../config";
import Cookies from "js-cookie";

const defalutData = {
  name: "",
  password: "",
};
export default function App() {
  const [formData, setFormData] = useState(defalutData);
  const [error, setError] = useState("");
  const { name, password } = formData;
  const navigate = useNavigate();

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value,
    }));
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(formData);
    setFormData(defalutData);
    checkCredentials(formData.name, formData.password);
  };

  async function checkCredentials(name: string, password: string) {
    const user = {
      name: name,
      password: password,
    };
    const res = await fetch(`${BASE_URL}/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user),
    });

    if (res.status == 200) {
      const data = await res.json();
      Cookies.set("token", data.token, { expires: 7  });
      navigate("/productlist");
    } else {
      setError("Wrong username or password");
    }
  }

  return (
    <div className="main-container">
      <h1>Log in</h1>
      <br />
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name: </label>
        <br />
        <input type="text" id="name" value={name} onChange={onChange} />

        <br />
        <label htmlFor="password">Password: </label>
        <br />
        <input
          type="password"
          id="password"
          value={password}
          onChange={onChange}
        />
        <br />
        <button type="submit">Log in</button>
        <p className="error">{error}</p>
      </form>
    </div>
  );
}
