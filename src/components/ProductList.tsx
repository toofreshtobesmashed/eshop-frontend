import useCart from "../hooks/useCart";
import useProducts from "../hooks/useProducts";
import { ReactElement } from "react";
import Product from "./Product";
import Header from "./Header";
import Footer from "./Footer";
import "./ProductList.css";
import { useState, useEffect } from "react";
import { BASE_URL } from "../config";
import Cookies from "js-cookie";
import { OrderDto, OrderPreview } from "../context/ProductsProvider";

const ProductList = () => {
  const [order, setOrder] = useState<OrderDto>({
    id: 0,
    isFinised: false,
    orderProducts: [],
    time: 0,
    totalItems: 0,
    totalPrice: 0,
  });

  useEffect(() => {
    loadOrder();
  }, []);

  async function loadOrder() {
    const load = async (): Promise<OrderDto> => {
      const data = await fetch(`${BASE_URL}/orders/current`, {
        method: "GET",
        headers: { Authorization: `Bearer ${Cookies.get("token")}` },
      })
        .then((res) => {
          console.log("objednavka bola zobrazena");
          return res.json();
        })
        .catch((err) => {
          if (err instanceof Error) console.log(err.message);
        });
      return data;
    };

    load().then((loadedOrder) => setOrder(loadedOrder));
  }

  const { products } = useProducts();
  let pageContent: ReactElement | ReactElement[] = <p>Loading...</p>;

  if (products?.length) {
    pageContent = products.map((product) => {
      const quantity = order.orderProducts.find(
        (item) => item.product.id === product.id
      )?.quantity;

      return (
        <Product
          onAdd={loadOrder}
          key={product.description}
          product={product}
          quantity={quantity}
        />
      );
    });
  }
  const content = (
    <>
      <Header totalItems={order.totalItems} totalPrice={order.totalPrice} />
      <main className="main main--products">{pageContent}</main>
      <Footer totalItems={order.totalItems} totalPrice={order.totalPrice} />
    </>
  );

  return content;
};

export default ProductList;
